//import Head from 'next/head';
import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import NavBar from '../components/NavBar';

export default function Home() {
  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/course",
    label: "Enroll now!"
  }

  return (
      <React.Fragment>
        <Banner dataProp={data} />
        <Highlights />
      </React.Fragment>
  )
}
