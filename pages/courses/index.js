import React, { useContext } from "react";
import CoursesData from "../../data/CoursesData";
import UserContext from "../../UserContext";
import Head from "next/head";

import { Table, Button } from "react-bootstrap";
import CourseCards from "../../components/CourseCards";
import Link from "next/link";

export default function index() {
  const { user } = useContext(UserContext);

  const courses = CoursesData.map((indivCourse) => {
      return <CourseCards key={indivCourse.id} courseProp={indivCourse} />
  });

  const courseRow = CoursesData.map((indivCourse) => {
    return (
      <tr key={indivCourse.id}>
        <td>{indivCourse.id}</td>
        <td>{indivCourse.name}</td>
        <td>{indivCourse.description}</td>
        <td>{indivCourse.price}</td>
        <td>{indivCourse.onOffer ? "open" : "closed"}</td>
        <td>{indivCourse.start_date}</td>
        <td>{indivCourse.end_date}</td>
        <td>
          
          <Button href="/updatecourse" className="bg-warning">Update</Button>
          <Button className="bg-danger">Disable</Button>
        </td>
      </tr>
    );
  });

  return user.isAdmin === true ? (
    <React.Fragment>
      <Head>
        <title>Courses Admin Dashboard</title>
      </Head>

      <h1>Course Dashboard</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Actions Taken</th>
          </tr>
        </thead>
        <tbody>{courseRow}</tbody>
      </Table>
    </React.Fragment>
  ) : (
    <React.Fragment>{courses}</React.Fragment>
  );
}
