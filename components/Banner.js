import { Button, Col, Jumbotron, Row } from "react-bootstrap";

function Banner() {
  return (
    <div>
      <Row>
        <Col>
          <Jumbotron>
            <h1>Zuitt Bootcamp 2021</h1>
            <p>Opportunities for everyone, everywhere</p>
            <Button variant="primary">Enroll Now!</Button>
          </Jumbotron>
        </Col>
      </Row>
    </div>
  );
}

export default Banner;
