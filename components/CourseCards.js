import React, { useEffect, useState } from 'react';
import PropTypes from "prop-types";
import { Button, Card } from 'react-bootstrap';

function CourseCards({courseProp}) {
    const{ name, description, price, start_date, end_date} = courseProp;
    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    const [isOpen, setIsOpen] = useState(true);

    function enroll(){
        setCount(count+1);
        setSeats(seats-1);
    }
    useEffect(() => {
        if (seats === 0) {
            setIsOpen(false);
        }
    }, [seats]);
    return (
        <div>
            <Card>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>
                        <span className="subtitle">Description</span>
                        <br />
                        {description}
                        <span className="subtitle">Price</span>
                        <br />
                        {price}
                        <br />
                        <span className="subtitle">Start Date: </span>
                        
                        {start_date}
                        <br/>
                        <span className="subtitle">End Date: </span>
                  
                        {end_date}
                        <br />
                        <span>Enrollees: {count}</span>
                        <br />
                        <span>Available Seats: {seats}</span>

                    </Card.Text>
                    {
                        isOpen ?
                        <Button className="bg-primary" onClick={enroll}>Enroll</Button>
                        :
                        <Button className="bg-secondary" disabled> Not Available</Button>
                    }
                </Card.Body>
            </Card>
        </div>
    );
}
CourseCards.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

export default CourseCards;